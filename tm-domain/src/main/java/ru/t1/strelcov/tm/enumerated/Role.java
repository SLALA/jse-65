package ru.t1.strelcov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {

    USER("User"),
    ADMIN("Administrator");

    private final String displayName;

    public static boolean isValidByName(final String name) {
        for (final Role role : values()) {
            if (role.toString().equals(name))
                return true;
        }
        return false;
    }

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
