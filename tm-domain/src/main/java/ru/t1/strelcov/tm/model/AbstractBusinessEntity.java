package ru.t1.strelcov.tm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.entity.IWBS;
import ru.t1.strelcov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import static ru.t1.strelcov.tm.enumerated.Status.NOT_STARTED;

@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractBusinessEntity extends AbstractEntity implements IWBS {

    @Column(nullable = false)
    @NotNull
    private String name;

    @Nullable
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @NotNull
    private Status status = NOT_STARTED;

    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @NotNull
    private Date created = new Date();

    @JoinColumn(name = "user_id", nullable = false)
    @ManyToOne
    @NotNull
    private User user;

    @Column(name = "start_date")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @Nullable
    private Date dateStart;

    public AbstractBusinessEntity(@NotNull User user, @NotNull String name) {
        this.user = user;
        this.name = name;
    }

    public AbstractBusinessEntity(@NotNull User user, @NotNull String name, @Nullable String description) {
        this.user = user;
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + " : " + name + " : " + created + " : " + user.getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AbstractBusinessEntity that = (AbstractBusinessEntity) o;
        return name.equals(that.name) && Objects.equals(description, that.description) && status == that.status && Objects.equals(created.getTime(), that.created.getTime()) && user.equals(that.user) && Objects.equals(Optional.ofNullable(dateStart).map(Date::getTime).orElse(null), Optional.ofNullable(that.dateStart).map(Date::getTime).orElse(null));
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, description, status, created.getTime(), user, Optional.ofNullable(dateStart).map(Date::getTime).orElse(null));
    }

}
