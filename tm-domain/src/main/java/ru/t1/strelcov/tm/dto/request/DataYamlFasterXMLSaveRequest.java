package ru.t1.strelcov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataYamlFasterXMLSaveRequest extends AbstractUserRequest {

    public DataYamlFasterXMLSaveRequest(@Nullable final String token) {
        super(token);
    }

}
