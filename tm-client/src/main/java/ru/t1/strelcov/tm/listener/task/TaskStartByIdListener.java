package ru.t1.strelcov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.model.TaskDTO;
import ru.t1.strelcov.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.util.TerminalUtil;

import static ru.t1.strelcov.tm.enumerated.Status.IN_PROGRESS;

@Component
public final class TaskStartByIdListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-start-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Start task by id.";
    }

    @Override
    @EventListener(condition = "@taskStartByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskDTO task = taskEndpoint.changeStatusByIdTask(new TaskChangeStatusByIdRequest(getToken(), id, IN_PROGRESS.name())).getTask();
        showTask(task);
    }

}
