package ru.t1.strelcov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.strelcov.tm.api.service.ITokenService;

@Service
@Getter
@Setter
@NoArgsConstructor
public class TokenService implements ITokenService {

    @Nullable
    private String token;

    public TokenService(@Nullable final String token) {
        this.token = token;
    }

}
