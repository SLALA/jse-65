package ru.t1.strelcov.tm.exception.entity;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class IncorrectPasswordException extends AbstractException {

    public IncorrectPasswordException() {
        super("Error: Password is incorrect.");
    }

}
