package ru.t1.strelcov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.request.DataJsonFasterXMLSaveRequest;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.event.ConsoleEvent;

@Component
public final class DataJsonSaveFasterXMLListener extends AbstractDataListener {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "data-json-save-fasterxml";
    }

    @Override
    @NotNull
    public String description() {
        return "Save entities data to json file.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonSaveFasterXMLListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA JSON SAVE]");
        dataEndpoint.saveJsonFasterXMLData(new DataJsonFasterXMLSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
