package ru.t1.strelcov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class DisplayCommandsListener extends AbstractListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Display commands.";
    }

    @Override
    @EventListener(condition = "@displayCommandsListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[COMMANDS]");
        final Collection<AbstractListener> commands = commandService.getCommands();
        for (final AbstractListener command : commands) {
            @NotNull final String name = command.name();
            if (name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
