package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void info(@Nullable String message);

    void commands(@Nullable String message);

    void errors(@Nullable Throwable e);

}
