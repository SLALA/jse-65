package ru.t1.strelcov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.strelcov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.strelcov.tm.api.endpoint.IUserEndpoint;
import ru.t1.strelcov.tm.configuration.ClientConfiguration;
import ru.t1.strelcov.tm.dto.model.UserDTO;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.marker.IntegrationCategory;

import java.util.List;

public class UserEndpointTest {

    @NotNull
    private static final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class);

    @NotNull
    private static final IAuthEndpoint authEndpoint = context.getBean(IAuthEndpoint.class);

    @NotNull
    private static final IUserEndpoint userEndpoint = context.getBean(IUserEndpoint.class);

    @Nullable
    private String token;

    @Before
    public void before() {
        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        userEndpoint.registerUser(new UserRegisterRequest(token, "test1", "pass1", Role.USER.name()));
    }

    @After
    public void after() {
        @NotNull final List<UserDTO> list = userEndpoint.listUser(new UserListRequest(token)).getList();
        for (@NotNull final UserDTO user : list) {
            if (user.getLogin().equals("test1") || user.getLogin().equals("test2"))
                userEndpoint.removeUserByLogin(new UserRemoveByLoginRequest(token, user.getLogin()));
        }
        authEndpoint.logout(new UserLogoutRequest());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void listUsersTest() {
        int size = userEndpoint.listUser(new UserListRequest(token)).getList().size();
        userEndpoint.registerUser(new UserRegisterRequest(token, "test2", "pass2", Role.USER.name()));
        Assert.assertEquals(size + 1, userEndpoint.listUser(new UserListRequest(token)).getList().size());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void changePasswordTest() {
        @NotNull final String token = authEndpoint.login(new UserLoginRequest("test1", "pass1")).getToken();
        Assert.assertNotNull(token);
        userEndpoint.changePassword(new UserChangePasswordRequest(token, "new pass"));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("test1", "pass1")));
        Assert.assertNotNull(authEndpoint.login(new UserLoginRequest("test1", "new pass")).getToken());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void registerUserTest() {
        int size = userEndpoint.listUser(new UserListRequest(token)).getList().size();
        userEndpoint.registerUser(new UserRegisterRequest(token, "test2", "pass2", Role.USER.name()));
        Assert.assertEquals(size + 1, userEndpoint.listUser(new UserListRequest(token)).getList().size());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void removeUserByLoginTest() {
        int size = userEndpoint.listUser(new UserListRequest(token)).getList().size();
        userEndpoint.removeUserByLogin(new UserRemoveByLoginRequest(token, "test1"));
        Assert.assertEquals(size - 1, userEndpoint.listUser(new UserListRequest(token)).getList().size());
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUserByLogin(new UserRemoveByLoginRequest(token, "admin")));
    }

    @Category(IntegrationCategory.class)
    @Test
    public void updateUserByLoginTest() {
        userEndpoint.updateUserByLogin(new UserUpdateByLoginRequest(token, "admin", "Name1", "Fam1", "email1"));
        Assert.assertEquals("Name1", authEndpoint.getProfile(new UserProfileRequest(token)).getUser().getFirstName());
        Assert.assertEquals("Fam1", authEndpoint.getProfile(new UserProfileRequest(token)).getUser().getLastName());
        Assert.assertEquals("email1", authEndpoint.getProfile(new UserProfileRequest(token)).getUser().getEmail());
    }

}
