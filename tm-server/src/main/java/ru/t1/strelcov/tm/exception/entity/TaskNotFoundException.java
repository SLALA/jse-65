package ru.t1.strelcov.tm.exception.entity;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error: Task not found.");
    }

}
