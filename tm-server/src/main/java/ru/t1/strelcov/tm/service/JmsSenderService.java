package ru.t1.strelcov.tm.service;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.service.IJmsSenderService;

import javax.jms.*;

public final class JmsSenderService implements IJmsSenderService {

    @NotNull
    private static final String QUEUE_NAME = "TM_LOGGER";

    @NotNull
    private final ConnectionFactory connectionFactory =
            new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL); // default broker URL is : tcp://localhost:61616

    @Override
    @SneakyThrows
    public void send(@NotNull final String json) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createQueue(QUEUE_NAME);
        @NotNull final MessageProducer producer = session.createProducer(destination);
        @NotNull final TextMessage message = session.createTextMessage(json);
        producer.send(message);
        producer.close();
        session.close();
        connection.close();
    }

}
