package ru.t1.strelcov.tm.api.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.strelcov.tm.model.AbstractEntity;

@NoRepositoryBean
public interface IRepository<E extends AbstractEntity> extends JpaRepository<E, String> {

}
