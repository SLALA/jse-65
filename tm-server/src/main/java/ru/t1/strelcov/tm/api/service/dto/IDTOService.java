package ru.t1.strelcov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.model.AbstractEntityDTO;

import java.util.List;

public interface IDTOService<E extends AbstractEntityDTO> {

    @NotNull
    List<E> findAll();

    void add(@Nullable final E entity);

    void addAll(@Nullable final List<E> list);

    void clear();

    @NotNull
    E findById(@Nullable final String id);

    @NotNull
    E removeById(@Nullable final String id);

}
