package ru.t1.strelcov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.strelcov.tm.api.repository.model.IRepository;
import ru.t1.strelcov.tm.api.service.model.IService;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.model.AbstractEntity;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    public abstract IRepository<E> getRepository();

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll() {
        @NotNull final IRepository<E> repository = getRepository();
        return repository.findAll();
    }

    @Transactional
    @SneakyThrows
    @Override
    public void add(@Nullable final E entity) {
        if (entity == null) return;
        @NotNull final IRepository<E> repository = getRepository();
        repository.save(entity);
    }

    @Transactional
    @SneakyThrows
    @Override
    public void addAll(@Nullable final List<E> list) {
        @Nullable List<E> listNonNull = Optional.ofNullable(list).map(l -> l.stream().filter(Objects::nonNull).collect(Collectors.toList())).orElse(null);
        if (listNonNull == null || listNonNull.size() == 0) return;
        @NotNull final IRepository<E> repository = getRepository();
        repository.saveAll(listNonNull);
    }

    @Transactional
    @SneakyThrows
    @Override
    public void clear() {
        @NotNull final IRepository<E> repository = getRepository();
        repository.deleteAll();
    }

    @SneakyThrows
    @NotNull
    @Override
    public E findById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final IRepository<E> repository = getRepository();
        return repository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public E removeById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final IRepository<E> repository = getRepository();
        final E entity = repository.findById(id).orElseThrow(EntityNotFoundException::new);
        repository.deleteById(id);
        return entity;
    }

}
