package ru.t1.strelcov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.model.UserDTO;
import ru.t1.strelcov.tm.enumerated.Role;

public interface IUserDTOService extends IDTOService<UserDTO> {

    @NotNull
    UserDTO add(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO add(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO add(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    UserDTO findByLogin(@Nullable String name);

    @NotNull
    UserDTO removeByLogin(@Nullable String name);

    @NotNull
    UserDTO updateById(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName, @Nullable String email);

    @NotNull
    UserDTO updateByLogin(@Nullable String login, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName, @Nullable String email);

    void changePasswordById(@Nullable String id, @Nullable String password);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
