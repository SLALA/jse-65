package ru.t1.strelcov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    void deleteByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}
