package ru.t1.strelcov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.strelcov.tm.api.repository.model.IBusinessRepository;
import ru.t1.strelcov.tm.api.service.model.IBusinessService;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyNameException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.exception.system.IncorrectSortOptionException;
import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    @NotNull
    public abstract IBusinessRepository<E> getRepository();

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        @NotNull final IBusinessRepository<E> entityRepository = getRepository();
        return entityRepository.findAllByUserId(userId);
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId, @Nullable final String sort) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(sort).orElseThrow(() -> {
            throw new IncorrectSortOptionException(sort);
        });
        if (!SortType.isValidByName(sort)) throw new IncorrectSortOptionException(sort);
        @NotNull final IBusinessRepository<E> entityRepository = getRepository();
        return entityRepository.findAllByUserId(userId, Sort.by(SortType.valueOf(sort).getDataBaseName()));
    }

    @Transactional
    @SneakyThrows
    @Override
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        @NotNull final IBusinessRepository<E> entityRepository = getRepository();
        entityRepository.deleteByUserId(userId);
    }

    @SneakyThrows
    @NotNull
    @Override
    public E findById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final IBusinessRepository<E> entityRepository = getRepository();
        return Optional.ofNullable(entityRepository.findFirstByUserIdAndId(userId, id)).orElseThrow(EntityNotFoundException::new);
    }

    @SneakyThrows
    @NotNull
    @Override
    public E findByName(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final IBusinessRepository<E> entityRepository = getRepository();
        return Optional.ofNullable(entityRepository.findFirstByUserIdAndName(userId, name)).orElseThrow(EntityNotFoundException::new);
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public E removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final IBusinessRepository<E> entityRepository = getRepository();
        @NotNull final E entity = Optional.ofNullable(entityRepository.findFirstByUserIdAndId(userId, id)).orElseThrow(EntityNotFoundException::new);
        entityRepository.deleteByUserIdAndId(userId, id);
        return entity;
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public E removeByName(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final IBusinessRepository<E> entityRepository = getRepository();
        @NotNull final E entity = Optional.ofNullable(entityRepository.findFirstByUserIdAndName(userId, name)).orElseThrow(EntityNotFoundException::new);
        entityRepository.deleteByUserIdAndName(userId, name);
        return entity;
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public E updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final IBusinessRepository<E> entityRepository = getRepository();
        @NotNull final E entity = Optional.ofNullable(entityRepository.findFirstByUserIdAndId(userId, id)).orElseThrow(EntityNotFoundException::new);
        entity.setName(name);
        entity.setDescription(description);
        entityRepository.save(entity);
        return entity;
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public E updateByName(@Nullable final String userId, @Nullable final String oldName, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(oldName).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final IBusinessRepository<E> entityRepository = getRepository();
        @NotNull final E entity = Optional.ofNullable(entityRepository.findFirstByUserIdAndName(userId, oldName)).orElseThrow(EntityNotFoundException::new);
        entity.setName(name);
        entity.setDescription(description);
        entityRepository.save(entity);
        return entity;
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public E changeStatusById(@Nullable final String userId, @Nullable final String id, @NotNull final Status status) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final IBusinessRepository<E> entityRepository = getRepository();
        @NotNull final E entity = Optional.ofNullable(entityRepository.findFirstByUserIdAndId(userId, id)).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS)
            entity.setDateStart(new Date());
        entityRepository.save(entity);
        return entity;
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public E changeStatusByName(@Nullable final String userId, @Nullable final String name, @NotNull final Status status) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final IBusinessRepository<E> entityRepository = getRepository();
        @NotNull final E entity = Optional.ofNullable(entityRepository.findFirstByUserIdAndName(userId, name)).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS)
            entity.setDateStart(new Date());
        entityRepository.save(entity);
        return entity;
    }

}
