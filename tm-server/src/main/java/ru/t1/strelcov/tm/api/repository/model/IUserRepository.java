package ru.t1.strelcov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findFirstByLogin(@NotNull String login);

    void deleteByLogin(@NotNull String login);

    boolean existsByLogin(@NotNull final String login);

}
