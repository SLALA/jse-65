package ru.t1.strelcov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.service.DataService;
import ru.t1.strelcov.tm.service.PropertyService;

import javax.annotation.PostConstruct;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class Backup implements Runnable {

    @NotNull
    private final static Integer INTERVAL = 30;

    @Autowired
    @NotNull
    private PropertyService propertyService;

    @Autowired
    @NotNull
    private DataService dataService;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @SneakyThrows
    @Override
    public void run() {
        save();
    }

    private void load() {
        dataService.loadBackup();
    }

    private void save() {
        dataService.saveBackup();
    }

    @PostConstruct
    public void init() {
        if (propertyService.getBackupStatus().equals("enabled")) {
            load();
            es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
        }
    }

}
