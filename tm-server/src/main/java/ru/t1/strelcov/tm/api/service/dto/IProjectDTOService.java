package ru.t1.strelcov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;

public interface IProjectDTOService extends IBusinessDTOService<ProjectDTO> {

    @NotNull
    ProjectDTO removeProjectWithTasksById(@Nullable String userId, @Nullable String projectId);

}
