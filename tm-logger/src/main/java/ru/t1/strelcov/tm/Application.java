package ru.t1.strelcov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.strelcov.tm.bootstrap.Bootstrap;
import ru.t1.strelcov.tm.configuration.LoggerConfiguration;

public class Application {

    public static void main(@Nullable String[] args) {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}
