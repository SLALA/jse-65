package ru.t1.strelcov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.strelcov.tm.api.IReceiverService;

import javax.jms.*;

@Service
public final class ReceiverService implements IReceiverService {

    @NotNull
    private static final String QUEUE_NAME = "TM_LOGGER";

    @Autowired
    @NotNull
    private ConnectionFactory connectionFactory;

    @Override
    @SneakyThrows
    public void receive(@NotNull final MessageListener listener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createQueue(QUEUE_NAME);
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }

}
