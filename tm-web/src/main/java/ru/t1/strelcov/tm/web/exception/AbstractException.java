package ru.t1.strelcov.tm.web.exception;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractException extends RuntimeException {

    public AbstractException() {
    }

    public AbstractException(@NotNull final String message) {
        super(message);
    }

    public AbstractException(@NotNull final Throwable cause) {
        super(cause);
    }

}
