package ru.t1.strelcov.tm.web.exception.entity;

import ru.t1.strelcov.tm.web.exception.AbstractException;

public final class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error: Entity not found.");
    }

}
