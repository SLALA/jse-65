package ru.t1.strelcov.tm.web.exception.empty;

import ru.t1.strelcov.tm.web.exception.AbstractException;

public final class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error: Name is empty.");
    }

}
