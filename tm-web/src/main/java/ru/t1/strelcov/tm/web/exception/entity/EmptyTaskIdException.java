package ru.t1.strelcov.tm.web.exception.entity;

import ru.t1.strelcov.tm.web.exception.AbstractException;

public final class EmptyTaskIdException extends AbstractException {

    public EmptyTaskIdException() {
        super("Error: Task id is empty.");
    }

}
