package ru.t1.strelcov.tm.web.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.strelcov.tm.web.api.service.IProjectService;
import ru.t1.strelcov.tm.web.enumerated.Status;
import ru.t1.strelcov.tm.web.model.Project;

@Controller
@RequestMapping("/project")
public class ProjectController {

    @NotNull
    @Autowired
    IProjectService projectService;

    @GetMapping("/list")
    public ModelAndView list() {
        return new ModelAndView("project-list", "projects", projectService.findAll());
    }

    @PostMapping("/create")
    public String add() {
        int number = projectService.findAll().size() + 1;
        projectService.add("Project " + number, "Project " + number);
        return "redirect:/project/list";
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(
            @NotNull @PathVariable("id") final String id) {
        @Nullable final Project project = projectService.findById(id);
        if (project == null) return list();
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    @NotNull
    @PostMapping("/edit/{id}")
    public String edit(
            @NotNull @ModelAttribute("project") final Project project,
            @NotNull final BindingResult result
    ) {
        projectService.add(project);
        return "redirect:/project/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(
            @NotNull @PathVariable("id") final String id) {
        projectService.deleteById(id);
        return "redirect:/project/list";
    }

}
