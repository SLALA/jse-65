package ru.t1.strelcov.tm.web.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.strelcov.tm.web.api.service.ITaskService;
import ru.t1.strelcov.tm.web.client.TaskEndpointClient;
import ru.t1.strelcov.tm.web.model.Task;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskEndpoint implements TaskEndpointClient {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Override
    @GetMapping("/findAll")
    public List<Task> findAll() throws Exception {
        return taskService.findAll();
    }

    @Nullable
    @Override
    @GetMapping("/findAllByProjectId/{projectId}")
    public List<Task> findAllByProjectId(@PathVariable("projectId") @NotNull final String projectId) {
        return taskService.findAllByProjectId(projectId);
    }

    @Override
    @PostMapping("/add")
    public void add(@RequestBody @NotNull final Task task) throws Exception {
        taskService.add(task);
    }

    @Override
    @PostMapping("/addAll")
    public void addAll(@RequestBody @NotNull final List<Task> list) throws Exception {
        taskService.addAll(list);
    }

    @Override
    @PostMapping("/addByName")
    public Task addByName(@RequestParam final String name, @RequestParam final String description) {
        return taskService.add(name, description);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public Task findById(@PathVariable("id") @NotNull final String id) throws Exception {
        return taskService.findById(id);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") final @NotNull String id) {
        taskService.deleteById(id);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear() {
        taskService.clear();
    }

}
