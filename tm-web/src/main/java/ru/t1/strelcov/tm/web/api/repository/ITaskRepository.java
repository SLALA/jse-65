package ru.t1.strelcov.tm.web.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.strelcov.tm.web.model.Task;

import java.util.List;

public interface ITaskRepository extends JpaRepository<Task, String> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String projectId);

    void deleteAllByProjectId(@NotNull String projectId);

}
