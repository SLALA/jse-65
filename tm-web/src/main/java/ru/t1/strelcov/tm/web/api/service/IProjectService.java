package ru.t1.strelcov.tm.web.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.web.model.Project;

import java.util.List;

public interface IProjectService {

    @NotNull
    List<Project> findAll();

    void add(@Nullable final Project entity);

    Project add(@NotNull final String name, @Nullable final String description);

    void addAll(@Nullable final List<Project> list);

    void clear();

    @Nullable
    Project findById(@Nullable final String id);

    @Nullable
    Project deleteById(@Nullable final String id);

}
