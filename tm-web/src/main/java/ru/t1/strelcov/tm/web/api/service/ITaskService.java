package ru.t1.strelcov.tm.web.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.web.model.Task;

import java.util.List;

public interface ITaskService {

    @NotNull
    List<Task> findAll();

    void add(@Nullable final Task entity);

    Task add(@NotNull final String name, @Nullable final String description);

    void addAll(@Nullable final List<Task> list);

    void clear();

    @Nullable
    Task findById(@Nullable final String id);

    @Nullable
    Task deleteById(@Nullable final String id);

    @NotNull
    List<Task> findAllByProjectId(@Nullable final String projectId);

}
