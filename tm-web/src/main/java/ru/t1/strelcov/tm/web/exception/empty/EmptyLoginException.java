package ru.t1.strelcov.tm.web.exception.empty;

import ru.t1.strelcov.tm.web.exception.AbstractException;

public final class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error: Login is empty.");
    }

}
